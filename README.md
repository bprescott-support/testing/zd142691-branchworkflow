# zd142691-branchworkflow

proof of concept protecting branches from MRs that don't follow the correct workflow

the approach uses 'guard jobs' that cause the CI to fail, and so prevent merging,
providing the project is set up for this.

# intended git workflow

- Developers fork feature branches off either master or dev.
- They make their changes, raise MRs to merge to dev.
- MRs are raised to merge from dev to release.
- MRs are raised to merge from release to master.

`feature -> dev -> release -> master`

This is, for example, a mechanism to control changes release to production, which
is built off master, and there's appropriate testing, approvals and controls in place
to ensure code is only merged into master at the right time.

The branches dev, release, and master are all protected in GitLab.

# project setup

## settings/general

Merge strategies

- Any of the merge strategies will work with this.

- Fast forward merging has a particular
  benefit because, once merged up, all the protected branches are identical.

![identical branches](assets/identical-branches.png)

Pipelines must succeed

- Tick 'pipelines must succeed' - this is required for this solution to work as intended.

![pipelines must succeed image](assets/pipelines-must-succeed.png)

Merge request approvals

- These have been selected as required for your organisation's processes.

## settings/repository

Default branch

- Default branch is probably best set to 'dev'

> All merge requests and commits will automatically be made against this branch

Protected branches.

- It would make sense to 
  - Protect dev, release, and master
  - Prevent pushes to any of them.
  - Set code owner approval for zero, one or more as required by your process.
  - Set the group under 'allowed' to merge' as required by your process.

![protected branches](assets/protected-branches.png)

## CI protection of release and master

The goal is to prevent MRs to release or master from inadvertently being merged.

`.gitlab-ci.yml` contains two guard jobs. These are set with conditions based on the
source and destination branches.

These jobs *always* fail. Based on the settings above - pipelines must succeed - this
is sufficient to protect the release and master branches.

If a MR is raised from

- dev to release
- release to master

Neither guard job gets created, and so neither fails.

Any other MR where the target is release or master will result in failed CI

- feature to master
- feature to release
- dev to master

## Note #1 - behaviour of illegal MRs

- Developer has a commit to add to the protected branches, and raises an 'illegal' MR 
  from their feature branch to master. CI fails, the MR can't be merged.
  It's either closed, or left open.
- New MR raised for that commit to dev. It's merged.
- dev is merged to release
- release is merged to master.
- At this point, master is consistent with what was proposed by the illegal MR.

The original MR, instead of being in a failed state, it now shows as merged, even
though the pipeline failed.  It'll be moved from 'open' or 'closed' categories
into 'merged'.

## Note #2 - use of rules to define guard jobs

I raised [an issue](https://gitlab.com/gitlab-org/gitlab/issues/194129) for this.

At this time, if you define the guard jobs with rules, the other jobs must have
a dummy rule, otherwise they don't get created when the guard job triggers.

Follow that issue to find out why this is.

GitLab CI can achieve the same behaviour using syntax other than rules, which
would be an alternative approach.

## results

For screenshots and details of how this workflow works in practise, 
[go to the results branch](../tree/results#results)
